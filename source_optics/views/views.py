# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# views.py - code immediately behind all of the web routes.  Renders pages, graphs, and charts.

import json
import traceback
from urllib.parse import parse_qs
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from source_optics.views.webhooks import Webhooks
from . import dataframes, reports
from . import graphs as graphs
from .scope import Scope

# =================================================================================================================
# GRAPH FRAGMENTS


def graph_participation(request):
    """
    Number of authors over time
    """
    return graphs.render_basic_plot(request, 'author_total')

def graph_files_changed(request):
    """
    Number of files changed per time slice, by repo or author
    """
    return graphs.render_advanced_plot(request, 'files_changed')

def graph_lines_changed(request):
    """
    Number of lines changed over time, by repo or author
    """
    return graphs.render_advanced_plot(request, 'lines_changed')

def graph_commits(request):
    """
    Number of commits over time, by repo or author
    """
    return graphs.render_advanced_plot(request, 'commit_total')

def graph_creates(request):
    """
    Number of files created over time, by repo or author
    """
    return graphs.render_advanced_plot(request, 'creates')

def graph_edits(request):
    """
    Number of files edited over time, by repo or author:
    """
    return graphs.render_advanced_plot(request, 'edits')

def graph_moves(request):
    """
    Number of files moved over time, by repo or author
    FIXME: currently not displayed in app due to some scanner-detection issues that need to be investigated
    """
    return graphs.render_advanced_plot(request, 'moves')

def graph_commit_size(request):
    """
    Average commits size over time, not plotted against repos or authors because a stacked chart does not
    make sense.  This COULD be done if we add a flag that selects a line chart vs a stacked area (FIXME).
    """
    return graphs.render_basic_plot(request, 'average_commit_size')

def graph_path_segment(request):
    """
    Activity graph for an individual file or path segment.
    """
    scope = Scope(request)
    top_authors = dataframes.top_authors_for_path(scope)
    df = dataframes.path_segment_series(scope, top_authors)
    scope.context['graph'] = graph_module.path_segment_plot(df=df, scope=scope, top_authors=top_authors)
    return render(request, 'graph.html', context=scope.context)

# =================================================================================================================
# OBJECT TRAVERSAL PAGES

def list_orgs(request):
    """
    List all organizations in the app.
    """
    scope = Scope(request, add_orgs_table=True)
    scope.context.update(dict(
        title = "Source Optics: index",
        mode = 'orgs'
    ))
    return render(request, 'orgs.html', context=scope.context)

def list_repos(request, org=None):
    """
    List all repositories for an org.
    """
    scope = Scope(request, org=org, add_repo_table=True)
    scope.context.update(dict(
        title = "Source Optics: %s organization" % scope.org.name,
        mode = 'repos'
    ))
    return render(request, 'repos.html', context=scope.context)

def repo_index(request, repo=None):
    """
    Show the homepage summary for a repo, which mostly links to graphs/stats.
    """
    scope = Scope(request, repo=repo)
    scope.context['mode'] = 'repo'
    return render(request, 'repo.html', context=scope.context)

def author_index(request, author=None):
    """
    Show the homepage for an author which mostly links to graphs/stats.
    """
    scope = Scope(request, author=author)
    scope.context['mode'] = 'author'
    return render(request, 'author.html', context=scope.context)

# =================================================================================================================
# DETAIL PAGES

def show_graphs(request):
    """
    For a given author OR repo, show all relevant graphs
    """
    scope = Scope(request)
    scope.context['mode'] = 'graphs'
    return render(request, 'graphs.html', context=scope.context)

def report_stats(request):
    """
    generates a partial graph which is loaded in the repo graphs page. more comments in graphs.py
    """
    scope = Scope(request)
    data = reports.author_stats_table(scope)

    scope.context.update(dict(
        title = "SourceOptics: stats view",
        author_count = len(data),
        table_json = json.dumps(data),
        mode = 'stats',
    ))

    # FIXME: should be repo_authors ? perhaps this will be standardized...
    return render(request, 'stats.html', context=scope.context)

def report_commits(request, org=None):
    # FIXME: how about a scope object?
    scope = Scope(request)
    data = reports.commits_feed(scope)
    page = data['page']

    assert scope.repo or scope.author

    # FIXME: this needs cleanup - move generic pagination support to a common function
    # TODO: title can come from commits_feed function.  Right now nothing else
    # needs immediate pagination so this can wait.

    scope.context.update(dict(
        title = "Source Optics: commit feed",
        table_json = json.dumps(data['results']),
        page_number = page.number,
        has_previous = page.has_previous(),
        mode = "feed"
    ))

    if scope.context['has_previous']:
        if scope.repo:
            scope.context['next_link'] = "/report/commits?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.next_page_number())
        elif scope.author:
            scope.context['next_link'] = "/report/commits?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.next_page_number())
    scope.context['has_next'] = page.has_next()
    if scope.context['has_next']:
        if scope.repo:
            scope.context['next_link'] = "/report/commits?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.next_page_number())
        elif scope.author:
            scope.context['next_link'] = "/report/commits?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.next_page_number())

    return render(request, 'commits.html', context=scope.context)

def report_files(request):
    """
    generates a browseable directory tree
    """
    scope = Scope(request)
    data = reports.files(scope)
    scope.context.update(dict(
        title = "SourceOptics: tree view",
        mode = 'files'
    ))
    return render(request, 'files.html', context=scope.context)

# =================================================================================================================
# UTILITY URLS - NON DJANGO FRAMEWORK REST (for those, see api.py)

@csrf_exempt
def webhook_post(request, *args, **kwargs):
    """
    Receive an incoming webhook from something like GitHub and potentially flag a source code repo for a future scan,
    using the code in webhooks.py
    """

    if request.method != 'POST':
        return redirect('index')

    try:
        query = parse_qs(request.META['QUERY_STRING'])
        token = query.get('token', None)
        if token is not None:
            token = token[0]
        Webhooks(request, token).handle()
    except Exception:
        traceback.print_exc()
        return HttpResponseServerError("webhook processing error")

    return HttpResponse("ok", content_type="text/plain")
