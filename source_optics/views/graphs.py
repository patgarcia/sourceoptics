# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# graphs.py - generate altair graphs as HTML snippets given panda dataframe inputs (see dataframes.py).  These are loaded
# as URL fragments, each filling in DIVs.

import functools
from .. models import Statistic, FileChange
from django.db.models import Sum

import random
import string
import altair as alt
from django import template
import json
from django.shortcuts import redirect, render
from . import dataframes, reports
from . import graphs as graph_module
from .scope import Scope

# tootltips for graphs that have been partitioned by author
AUTHOR_TIME_SERIES_TOOLTIPS = ['day','author','commit_total', 'lines_changed', 'files_changed', 'longevity', 'days_since_seen' ]

# tooltip fields for basic graphs (no partitions)
TIME_SERIES_TOOLTIPS = ['day','commit_total', 'lines_changed', 'files_changed', 'author_total' ]

# FIXME: add tooltips for graphs that were partitioned by repo?

# javascript needed to wrap an altair chart
TEMPLATE_CHART = """
<div id="{output_div}"></div>
    <script type="text/javascript">
    var spec = {spec};
    var embed_opt = {embed_opt};
    function showError({output_div}, error){{
        {output_div}.innerHTML = ('<div class="error">'
                        + '<p>JavaScript Error: ' + error.message + '</p>'
                        + "<p>This usually means there's a typo in your chart specification. "
                        + "See the javascript console for the full traceback.</p>"
                        + '</div>');
        throw error;
    }}
    const {output_div} = document.getElementById('{output_div}');
    vegaEmbed("#{output_div}", spec, embed_opt)
      .catch(error => showError({output_div}, error));
    </script>
"""

def render_chart(chart):
    """
    wraps an altair chart ("chart") with the required javascript/html to display that chart
    """
    spec = chart.to_dict()
    output_div = '_' + ''.join(random.choices(string.ascii_letters + string.digits, k=7))
    embed_opt = {"mode": "vega-lite", "actions": False}
    c = template.Context()
    return template.Template(TEMPLATE_CHART.format(output_div=output_div, spec=json.dumps(spec), embed_opt=json.dumps(embed_opt))).render(c)

@functools.lru_cache(maxsize=64)
def get_stat(repo, author, start, end, aspect):
   """
   Used when building a legend, this looks up a statistic for a given author to determine the sort order.
   FIXME: Realistically, we should be passing this in from dataframes.py code and keeping the visualization layer dumb.
   """

   value = Statistic.objects.filter(
       author=author,
       repo=repo,
       interval='DY',
       start_date__range=(start,end)
   ).aggregate(
       lines_changed=Sum('lines_changed'),
       commit_total=Sum('commit_total')
   )[aspect]

   if value is None:
       return -10000

   return value

def time_plot(scope=None, df=None, repo=None, y=None, by_author=False, top=None, aspect=None):
    """
    Generates a time series area plot from a dataframe.  Nearly all graphs go through here.
    FIXME: the chart type should be a parameter - this will not always return altair charts.

    :param df: a pandas dataframe
    :param y: the name of the y axis from the dataframe
    :param top: the legend, used for the top authors plot sorting, as a list of authors
    :param aspect: the aspect the chart was limited by
    :param author: true if the chart is going to be showing authors vs the whole team together
    :return: chart HTML
    """
    assert df is not None
    assert scope is not None
    start = scope.start
    end = scope.end
    repo = scope.repo

    if top:
        assert start is not None
        assert end is not None
        assert repo is not None
        # sort top authors by the statistic we filtered them by
        top = reversed(sorted(top, key=lambda x: get_stat(repo, x, start, end, aspect)))
        top = [ x.get_display_name() for x in top ]
        top.append('OTHER')

    tooltips = TIME_SERIES_TOOLTIPS
    if by_author:
        tooltips = AUTHOR_TIME_SERIES_TOOLTIPS
    if scope.author:
        tooltips.append('repo')

    alt.data_transformers.disable_max_rows()

    if by_author:
        df = df.sort_values(by=['author'])
        chart = alt.Chart(df, height=600, width=600).mark_area( opacity=0.9).encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            color=alt.Color('author', sort=top),
            tooltip=tooltips
        ).interactive()
    elif not scope.multiple_repos_selected():
        chart = alt.Chart(df, height=600, width=600).mark_line().encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            tooltip=tooltips
        ).interactive()
    else:
        # multiple repos
        chart = alt.Chart(df, height=600, width=600).mark_line().encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            color=alt.Color('repo'),
            tooltip=tooltips
        ).interactive()

    return render_chart(chart)

def path_segment_plot(df, scope, top_authors):
    """
    Path segment charts work somewhat differently as they don't use Statistics objects. There's a bit more duplicated
    code that still needs to find the right shape and home yet (FIXME).
    """

    assert top_authors is not None

    def get_author_stat(repo, author):

        value = FileChange.objects.filter(
            commit__author=author,
            commit__repo=scope.repo,
            commit__commit_date__range=(scope.start, scope.end),
            file__path=scope.path
        )
        if scope.file:
            value = value.filter(file__name=scope.file)
        value = value.count()
        if value is None:
            return -10000
        return value


    # sort top authors by the statistic we filtered them by
    top = reversed(sorted(top_authors, key=lambda x: get_author_stat(scope, x)))

    top = [ x.get_display_name() for x in top ]
    top.append('OTHER')


    # a basic plot of directory activity that can be improved later as we choose to update it.
    tooltips = ['date:T','commits','author']
    chart = alt.Chart(df, height=150, width=300).mark_area().encode(
        x=alt.X('date:T', axis=alt.Axis(title='date', format=("%b %Y")), scale=alt.Scale(zero=False, clamp=True)),
        y=alt.Y('commits', scale=alt.Scale(zero=True)),
        color=alt.Color('author'), # sort=top),
        tooltip=tooltips
    ).interactive()
    return render_chart(chart)

def get_basic_plot(scope, df, y):
    """
    A basic plot draws a graph for the whole repo, and is not segmented by author or repo.
    """
    return graph_module.time_plot(df=df, scope=scope, y=y, top=None)

def get_advanced_plot(scope, df, top, y, aspect1='repo', aspect2='commit_total'):
    """
    Advanced plots are segmented by the author and repo, depending on context.
    """
    if scope.multiple_repos_selected():
        return graph_module.time_plot(df=df, scope=scope, y=y, top=top, aspect=aspect1)
    else:
        return graph_module.time_plot(df=df, scope=scope, y=y, top=top, by_author=True, aspect=aspect2)

def render_basic_plot(request, y):
    """
    View-level code: Renders a basic line chart from a request.
    """
    scope = Scope(request)
    (df, _) = dataframes.get_basic_dataframe(scope)
    scope.context['graph'] = get_basic_plot(scope, df, y=y)
    return render(request, 'graph.html', context=scope.context)

def render_advanced_plot(request, y):
    """
    View-level code: Renders a stacked area chart segmented by repo or user, based on context.
    """
    scope = Scope(request)
    (df, top) = dataframes.get_advanced_dataframe(scope)
    scope.context['graph'] = get_advanced_plot(scope, df, top, y=y)
    return render(request, 'graph.html', context=scope.context)